#ifndef SAMSUNGTV_H
#define SAMSUNGTV_H

#include <QObject>
#include <QDebug>
#include "abstractobject.h"

class SamsungTV : public AbstractObject
{
    Q_OBJECT
public:
    Q_INVOKABLE SamsungTV(QObject *parent = nullptr);

signals:

public slots:
    void PowerOn();
};
Q_DECLARE_METATYPE(SamsungTV)
#endif // SAMSUNGTV_H
