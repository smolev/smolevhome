#ifndef ABSTRACTOBJECT_H
#define ABSTRACTOBJECT_H

#include <QObject>
#include <QDebug>
#include <QHash>
#include <QString>
#include "defaults.h"

class AbstractObject : public QObject
{
    Q_OBJECT
public:
    explicit AbstractObject(QObject *parent = nullptr);
    AbstractObject(const AbstractObject &);
    const char *className() const;
    //const QHash<int/*Defaults::PhraseID*/, QString> &get_methods() const;
    //const QHash<int/*Defaults::PhraseID*/, QString> &get_props() const;
    //const QHash<QString,QString> &get_actions() const;
    //const QString &get_i18n() const;

protected:
    QHash<int/*Defaults::PhraseID*/, QString> methods_ref;
    QHash<int/*Defaults::PhraseID*/, QString> props_ref;
    QHash<QString,QString> actions_ref;
    QString i18n;

signals:

public slots:
    void testMethod();
};

#endif // ABSTRACTOBJECT_H
