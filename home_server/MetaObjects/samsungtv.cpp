#include "samsungtv.h"

SamsungTV::SamsungTV(QObject *parent) : AbstractObject(parent)
{
    props_ref[Defaults::ObjectMethod]="Power";
    methods_ref[Defaults::ObjectMethod]="PowerOff,PowerOn";
    actions_ref["Power"]="On,Off";
    i18n="телевизор";

}
void SamsungTV::PowerOn(){
    qDebug() << "PowerOn";
}
