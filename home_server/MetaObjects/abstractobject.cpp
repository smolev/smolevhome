#include "abstractobject.h"

AbstractObject::AbstractObject(QObject *parent) : QObject(parent)
{

}
AbstractObject::AbstractObject(const AbstractObject &){

}

void AbstractObject::testMethod(){
    qDebug() << "call testMethod in AbstractObject";
}

const char *AbstractObject::className() const{
    return metaObject()->className();
}
/*
const QHash<int, QString> &AbstractObject::get_methods() const{
    return &methods_ref;
}
const QHash<int, QString> &AbstractObject::get_props() const{
    return &props_ref;
}
const QHash<QString,QString> &AbstractObject::get_actions() const{
    return &actions_ref;
}
const QString &AbstractObject::get_i18n() const{
    return &i18n;
}
*/
