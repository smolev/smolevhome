#include "aicodebase.h"

AICodeBase::AICodeBase(Defaults *d, QObject *parent) : QObject(parent)
{
    defaults=d;
    qRegisterMetaType<SamsungTV>("SamsungTV");
    QStringList *places = defaults->get_places();
    foreach (const QString &place, *places) {
        QStringList *objs = defaults->objectsForPlace(place);
        foreach (const QString &obj_name, *objs) {
            objects[place].insert(obj_name,createObject(obj_name));//obj_name
        }
        delete objs;
    }
    delete places;
    qDebug() << objects;
    //objects
}

AICodeBase::~AICodeBase(){
    delete lang;
}

AbstractObject *AICodeBase::createObject(const QString &name){
    QString *class_name = defaults->classNameByObject(name);
    int id = QMetaType::type(class_name->toLatin1());
    delete class_name;
    if(id==-1 || id == QMetaType::UnknownType) return nullptr;
    AbstractObject *objectPtr = (AbstractObject *)QMetaType::create(id);
    return objectPtr;
}

void AICodeBase::superSwitch(Defaults::PhraseID super_method, const QHash<Defaults::GroupType,QString> &context){
    switch(super_method){
        case Defaults::ObjectMethod:{
            QString place=context[Defaults::Places];
            QString method = context[Defaults::Methods];
            QList<QString> obj_names = objects[place].keys();
            foreach (const QString &obj_name, obj_names) {
                AbstractObject *obj = objects[place].value(obj_name);
                if(obj && context[Defaults::Objects] == obj_name){
                    QMetaObject::invokeMethod(obj, method.toLatin1(), Qt::DirectConnection);
                }
            }
        }
    }
}

