QT += core multimedia network sql
QT -= gui

CONFIG += c++11

TARGET = home_server
CONFIG += console
CONFIG -= app_bundle
LIBS += -L/usr/local/Cellar/protobuf/3.3.0/lib -lprotobuf

TEMPLATE = app
INCLUDEPATH += /usr/local/Cellar/protobuf/3.3.0/include

SOURCES += main.cpp \
    nooliteserver.cpp \
    yarecognise.cpp \
    proto/tts.pb.cc \
    proto/basic.pb.cc \
    proto/ttsbackend.pb.cc \
    proto/voiceproxy.pb.cc \
    roothomeserver.cpp \
    yaspeech.cpp \
    statemachinedialog.cpp \
    aicodebase.cpp \
    MetaObjects/samsungtv.cpp \
    MetaObjects/abstractobject.cpp \
    defaults.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    nooliteserver.h \
    yarecognise.h \
    proto/tts.pb.h \
    proto/basic.pb.h \
    proto/ttsbackend.pb.h \
    proto/voiceproxy.pb.h \
    roothomeserver.h \
    yaspeech.h \
    statemachinedialog.h \
    aicodebase.h \
    MetaObjects/samsungtv.h \
    MetaObjects/abstractobject.h \
    defaults.h

OTHER_FILES += \
    proto/src/basic.proto \
    proto/src/tts.proto \
    proto/src/ttsbackend.proto \
    proto/src/voiceproxy.proto

STATECHARTS +=

DISTFILES +=
