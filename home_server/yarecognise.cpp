#include "yarecognise.h"

YaRecognise::YaRecognise(QObject *p) : QThread()
{
    Q_UNUSED(p)
    current_state = Unconnected;
    //gender
    biometry["male"] = "мужчина";
    biometry["female"] = "женщина";
    //group
    biometry["c"] = "ребенок (ориентировочный возраст — до 14 лет)";
    biometry["ym"] = "юноша (14—20 лет)";
    biometry["yf"] = "девушка (14—20 лет)";
    biometry["am"] = "мужчина (20—55 лет)";
    biometry["af"] = "женщина (20—55 лет)";
    biometry["sm"] = "мужчина старше 55 лет";
    biometry["sf"] = "женщина старше 55 лет";
    moveToThread(this);
}

YaRecognise::~YaRecognise(){
    delete m_pTcpSocket;
    delete buffer;
    delete audioRecorder;
}

void YaRecognise::run(){
    buffer = new QByteArray;
    m_pTcpSocket = new QTcpSocket(this);
    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(m_pTcpSocket, SIGNAL(disconnected()), SLOT(slotDisconnected()));
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(slotError(QAbstractSocket::SocketError)));

    audioRecorder = new QAudioRecorder(this);
    QAudioEncoderSettings audioSettings;
    //audioSettings.setCodec("audio/mpeg");
    audioSettings.setCodec("audio/pcm");
    //audioSettings.setQuality(QMultimedia::NormalQuality);
    audioSettings.setBitRate(16);
    audioSettings.setSampleRate(16000);
    audioSettings.setChannelCount(1);
    audioSettings.setEncodingMode(QMultimedia::ConstantBitRateEncoding);
    audioRecorder->setEncodingSettings(audioSettings);
    audioProbe = new QAudioProbe();
    connect(audioProbe,&QAudioProbe::audioBufferProbed,this,&YaRecognise::processAudioBuffer);
    audioProbe->setSource(audioRecorder);
    //audioRecorder->setOutputLocation(QUrl::fromLocalFile("recorded-probe-samp.wav"));
    audioRecorder->record();
    exec();
}

void YaRecognise::slotReadyRead(){
    //*buffer = m_pTcpSocket->readAll();
    QByteArray size = m_pTcpSocket->readLine();
    if(size.indexOf("Switching Protocols")!=-1){
        *buffer = m_pTcpSocket->readAll();
        emit log("Yandex Switching Protocols");
        /*

        m_pTcpSocket->write(QString::number( wav.size(), 16 ));
        m_pTcpSocket->write("\r\n");
        */
        VoiceProxyProtobuf::ConnectionRequest r;
        r.set_protocolversion(1);
        r.set_speechkitversion("");
        r.set_servicename("asr_dictation");
        r.set_uuid(QUuid::createUuid().toString().remove("-").remove("{").remove("}").toStdString());
        r.set_apikey("1578a96c-8f40-4861-9bd4-4a65e4e0400f");
        r.set_applicationname("SmolevHome");
        r.set_device("embeded");
        r.set_coords("0,0");
        r.set_topic("queries"); //queries
        r.set_lang("ru-RU");
        r.set_format("audio/x-pcm;bit=16;rate=16000");

        VoiceProxyProtobuf::AdvancedASROptions *ad = new VoiceProxyProtobuf::AdvancedASROptions();
        ad->set_partial_results(true);
        ad->set_biometry("group,gender");
        r.set_allocated_advancedasroptions(ad);

        std::string data = r.SerializeAsString();
        writeData(data);
    }
    else{
        bool ok=false;
        size.chop(2);
        int message_size = size.toInt(&ok,16);
        if(message_size>700)
            qDebug() << "size: " << size << " / " << message_size;
        if(!ok){
            message_size=0;
            buffer->clear();
            m_pTcpSocket->readAll();
            qDebug() << "error, clear";
        }
        else{
            *buffer = m_pTcpSocket->read(message_size);
            if(buffer->size() < message_size){
                qDebug() << "SOCKET: Этот день настал";
                return;
            }
            //qDebug() << "data: " << *buffer;
            bufferProccess();
        }
    }
}

void YaRecognise::bufferProccess(){
    std::string stdString(buffer->constData(), buffer->length());
    //qDebug() << buffer->size();
    if(current_state != CanSendAudio){
        BasicProtobuf::ConnectionResponse r;
        r.ParseFromString(stdString);
        if(r.responsecode()==200){
            current_state = CanSendAudio;
            //sendChunks();
            qDebug() << " >listen";
        }
        else{
            qDebug() << "response code: " << r.responsecode() << QString::fromStdString(r.message());
        }
    }
    else{
        VoiceProxyProtobuf::AddDataResponse r;
        if(r.ParseFromString(stdString)){
            if(r.responsecode()!=200){
                qDebug() << "response code: " << r.responsecode();
            }
            if(r.endofutt()){ //конечный результат
                for(int i=0;i<r.bioresult_size();i++){
                    QString classname = QString::fromStdString(r.bioresult(i).classname());
                    QString tag = QString::fromStdString(r.bioresult(i).tag());
                    qDebug() << biometry[classname] << ":" << r.bioresult(i).confidence() << ":" << tag;
                }
                qDebug() << "конечный результат";
                if(r.recognition_size()>0){
                    QString str=QString::fromStdString(r.recognition(0).normalized());
                    emit say(str);
                }
            }
            else{//промежуточный
                //qDebug() << "промежуточный результат";
            }
            for(int i=0;i<r.recognition_size();i++){
                qDebug() << r.recognition(i).confidence() << ":" << QString::fromStdString(r.recognition(i).normalized());
            }
        }
        else{
            qDebug() << "WTF???";
            qDebug() << "data: " << *buffer;
        }
    }
}
void YaRecognise::writeData(const std::string &data){
    m_pTcpSocket->write(QString::number(data.size(), 16).toUtf8());
    m_pTcpSocket->write("\r\n");
    m_pTcpSocket->write(data.data(),data.size());
}
void YaRecognise::processAudioBuffer(const QAudioBuffer &audioBuffer){
    //qDebug() << "audio: " << audioBuffer.byteCount();
    //audioBuffer.data();
    //std::string stdString(audioBuffer.constData<char>(), audioBuffer.byteCount());
    //QByteArray ba(audioBuffer.constData<char>(), audioBuffer.byteCount());

    sendChunks(audioBuffer);
}
void YaRecognise::sendChunks(const QAudioBuffer &wav){
    if(current_state == CanSendAudio){
        std::string stdString(wav.constData<char>(), wav.byteCount());
        audio_output.append(stdString);
        if(audio_output.length() > 200){
            VoiceProxyProtobuf::AddData d;
            d.set_audiodata(audio_output);
            audio_output.clear();
            d.set_lastchunk(false);
            writeData(d.SerializeAsString());
            //qDebug() << "sended";
        }
    }
    else if(m_pTcpSocket->state() == QAbstractSocket::UnconnectedState){
        m_pTcpSocket->connectToHost("asr.yandex.net",80);
    }
}

void YaRecognise::slotError(QAbstractSocket::SocketError err)
{
    current_state = Unconnected;
    QString strError =
        "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
                     QString(m_pTcpSocket->errorString())
                    );
    emit log(strError);
}

void YaRecognise::slotConnected()
{
    current_state = Connecting;
    //emit log("Yandex connectd");
    m_pTcpSocket->write("GET /asr_partial HTTP/1.1\r\n");
    m_pTcpSocket->write("User-Agent: KeepAliveClient\r\n");
    m_pTcpSocket->write("Host: asr.yandex.net:80\r\n");
    m_pTcpSocket->write("Upgrade: dictation\r\n\r\n");
}
void YaRecognise::slotDisconnected(){
    current_state = Unconnected;
    qDebug() << "yandex disconnected";
}
