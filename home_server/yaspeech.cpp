#include "yaspeech.h"

YaSpeech::YaSpeech(QObject *parent) : QThread()
{
    Q_UNUSED(parent)
    moveToThread(this);
    voice="ermil";
    emotion="good";
    cache_path = "cache/%1-%2-%3.wav";
    QDir dir(cache_path.split("/")[0]);
    if (!dir.exists()) {
        emit log("make cache dir");
        dir.mkpath(".");
    }
}
YaSpeech::~YaSpeech(){
    //delete m_webSocket;
    delete m_WebCtrl;
}

void YaSpeech::run(){
    m_WebCtrl = new QNetworkAccessManager;
    connect(m_WebCtrl, &QNetworkAccessManager::finished,this, &YaSpeech::ttsDownloaded);
    audio = new QSound("fileName.wav");
    exec();
}

void YaSpeech::say(const QString &msg){
    QString simplified = msg.simplified();
    //https://tts.voicetech.yandex.net/generate?format=mp3&lang=ru-RU&speaker=ermil&emotion=good&key=1578a96c-8f40-4861-9bd4-4a65e4e0400f
    emit log(simplified);
    QString md5 = QString(QCryptographicHash::hash(simplified.toUtf8(),QCryptographicHash::Md5).toHex());
    QString path=cache_path.arg(voice).arg(emotion).arg(md5);
    //emit log(path);
    if(QFileInfo::exists(path)){
        emit log("from cache");
        audio->play(path);
    }
    else{
        QUrl url("https://tts.voicetech.yandex.net/generate");
        QUrlQuery query;
        query.addQueryItem("format", "wav");
        query.addQueryItem("lang", "ru-RU");
        query.addQueryItem("speaker", voice);
        query.addQueryItem("emotion", emotion);
        query.addQueryItem("key", "1578a96c-8f40-4861-9bd4-4a65e4e0400f");
        query.addQueryItem("text", simplified);
        url.setQuery(query.query());
        qDebug() << url.toEncoded();
        QNetworkRequest request(url);
        m_WebCtrl->get(request);
    }
}
void YaSpeech::ttsDownloaded(QNetworkReply* pReply) {
    QByteArray mp3;
    mp3 = pReply->readAll();
    QString text = pReply->url().query().split("text=")[1];
    QString md5 = QString(QCryptographicHash::hash(text.toUtf8(),QCryptographicHash::Md5).toHex());
    QString str="downloaded: %1";
    emit log(str.arg(mp3.size()));
    QString path=cache_path.arg(voice).arg(emotion).arg(md5);
    QFile newDoc(path);
    if(newDoc.open(QIODevice::WriteOnly)){
        newDoc.write(mp3);
    }
    newDoc.close();
    QSound::play(path);
    pReply->deleteLater();
    //emit downloaded();
}

