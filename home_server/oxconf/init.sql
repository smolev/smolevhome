CREATE TABLE `Classes` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`qt_class`	TEXT NOT NULL UNIQUE,
	`methods`	TEXT,
	`props`	TEXT,
	`phrase_id`	INTEGER NOT NULL
);

CREATE TABLE `Objects` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`var_name`	TEXT NOT NULL UNIQUE,
	`qt_class`	TEXT NOT NULL,
	`place`	TEXT NOT NULL,
	`visible_name`	TEXT NOT NULL,
	`config`	BLOB
);

CREATE TABLE `Refs` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`refs_arr`	TEXT NOT NULL,
	`some`	TEXT NOT NULL UNIQUE
);

CREATE TABLE `Settings` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`key`	TEXT NOT NULL,
	`value`	TEXT NOT NULL,
	`obj_name`	TEXT NOT NULL
);

CREATE TABLE `i18n` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`key`	TEXT NOT NULL UNIQUE,
	`translation`	TEXT NOT NULL,
	`group_type`	INTEGER NOT NULL
);

INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('On','включи,включить,включай', 101 ); --Actions

INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('Off','выключи,отключи,выключить,выключай', 101 ); --Actions

INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('PowerOn','включи,включить,включай', 102 ); --Methods

INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('PowerOff','выключи,отключи,выключить,выключай', 102 ); --Methods

INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('Power','питание', 103 ); --Props

-- TEST
INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('playroom','детская,детской', 104 ); --Places

INSERT OR REPLACE INTO i18n (key,translation,group_type)
VALUES ('kitchen','кухня,кухне', 104 ); --Places
