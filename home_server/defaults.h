#ifndef DEFAULTS_H
#define DEFAULTS_H

#include <QObject>
#include <QString>
#include <QHash>
#include <QStringList>
#include <QSettings>
#include <QDirIterator>
#include <QDebug>
#include <QTextCodec>

#include <QSqlDatabase>
#include <QSqlQuery>
#include "MetaObjects/abstractobject.h"

#define OXCONFPATH QString("../home_server/oxconf/")

class Defaults : public QObject
{
    Q_OBJECT
public:
    enum PhraseID { ObjectMethod };
    Q_ENUM(PhraseID)
    enum GroupType {Actions=101, Methods, Props, Places, Objects, MetaObject};Q_ENUM(GroupType)
    explicit Defaults(QObject *parent = nullptr);
    bool contains(const QString &token);
    QList<PhraseID> *get_phrases() const;
    QList<GroupType> *phrase_keys(Defaults::PhraseID key) const;
    QStringList *tokensForObject(const QString &obj) const;
    QStringList *objectsForGroup(Defaults::GroupType g) const;
    QStringList *get_places() const;
    QStringList *objectsForPlace(const QString &place_name) const;
    QString *classNameByObject(const QString &obj) const;

signals:

public slots:

private:
    QSqlDatabase db;

    QHash<PhraseID,QList<GroupType> > phrases;
    //QHash<QString,QHash<QString,QString> > object_info;
    //QHash<QString/*playroom,...*/,QStringList/*BigTV,...*/> objects_by_place;
    //QHash<QString/*Objects,...*/,QStringList/*BigTV,...*/> objects_by_group;
    //QHash<QString/*Places,...*/,QStringList/*телевизор, ...*/> tokens_by_group_lang;
    //QHash<QString/*BigTV,...*/,QStringList/*телевизор, ...*/> tokens_by_object_lang;
    QStringList tokens_lang;
};
Q_DECLARE_METATYPE(Defaults::PhraseID)
#endif // DEFAULTS_H
