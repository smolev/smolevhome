#include "statemachinedialog.h"

StateMachineDialog::StateMachineDialog(Defaults *d, QObject *parent) : QObject(parent){
    defaults = d;
    //обход папок с конфигами
    //lang->setPath(QSettings::IniFormat,,path);
}

StateMachineDialog::~StateMachineDialog(){
    //delete lang;
}

void StateMachineDialog::user_say(const QString &msg){
    last_context[Defaults::Places]="playroom";

    QStringList tokens = msg.split(" ");

    foreach (const QString &value, tokens)
        if(!defaults->contains(value))
            tokens.removeAll(value);
    tokens.removeDuplicates();
    //qDebug() << tokens;

    QHash<Defaults::PhraseID, QHash<Defaults::GroupType,QString> > phrases_variant;
    QList<Defaults::PhraseID> *phrases = defaults->get_phrases();
    //qDebug() << *phrases;
    foreach (const Defaults::PhraseID &phrase, *phrases){
        QStringList tokens_dup(tokens);
        QList<Defaults::GroupType> *groups = defaults->phrase_keys(phrase);//lang->value(key1.arg(phrase)).toStringList();
        qDebug() << *groups;

        foreach (const Defaults::GroupType &group, *groups){ //Places, Objects ...
            bool founded=false;
            QStringList *keys = defaults->objectsForGroup(group); //lang->childKeys();
            qDebug() << group << *keys;
            foreach (const QString &key2, *keys){ //BigTV, SamsungTV, playroom
                QStringList *white_words=defaults->tokensForObject(key2);//lang->value(key2).toStringList();
                foreach (const QString &value, tokens_dup) //телевизоры, телевизор ...
                    if(white_words->contains(value)){
                        QStringListIterator toDelete(*white_words);
                        while(toDelete.hasNext()){tokens_dup.removeAll(toDelete.next());} //удаление всех слов списка

                        QHash<Defaults::GroupType,QString> *context = &phrases_variant[phrase];
                        context->insert(group,key2);

                        founded=true;
                        break;
                    }
                delete white_words;
                if(founded)break;
            }
            if(!founded){ //эврестический поиск недостающего контекста
                if(last_context.contains(group)){
                    QHash<Defaults::GroupType,QString> *context = &phrases_variant[phrase];
                    context->insert(group,last_context[group]);
                }
                //проверить что такого объекта\метода может больше нигде нет
            }
            delete keys;
        }
        if(tokens_dup.size() > 0 || phrases_variant.value(phrase).size() != groups->size()){
            phrases_variant.remove(phrase);
        }
        //проверить на совместимость объектов\методов
        delete groups;
    }

    if(phrases_variant.keys().size()>0){
        Defaults::PhraseID founded_phrase = phrases_variant.keys().first();
        last_context = phrases_variant[founded_phrase];
        qDebug() << last_context;
        emit function(founded_phrase,last_context);
    }
    else{
        qDebug() << "Не удалось найти команду";
    }

    delete phrases;
}
