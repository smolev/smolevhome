#ifndef AICODEBASE_H
#define AICODEBASE_H

#include <QObject>
#include <QDebug>
#include <QMetaObject>
#include <QMetaType>
#include <QTextCodec>
#include <QSettings>
#include <QStringList>
#include "MetaObjects/samsungtv.h"
#include "MetaObjects/abstractobject.h"
#include "defaults.h"

class AICodeBase : public QObject
{
    Q_OBJECT
public:
    explicit AICodeBase(Defaults *d, QObject *parent = nullptr);
    ~AICodeBase();

signals:

public slots:
    void superSwitch(Defaults::PhraseID super_method, const QHash<Defaults::GroupType, QString> &context);

protected:
    AbstractObject *createObject(const QString &name);

private:
    QSettings *lang;
    QHash<QString, QHash<QString,AbstractObject*> > objects;
    Defaults *defaults;
};

#endif // AICODEBASE_H
