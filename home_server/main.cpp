#include <QCoreApplication>
#include "roothomeserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    RootHomeServer home;
    home.start();

    return a.exec();
}
