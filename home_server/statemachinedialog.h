#ifndef STATEMACHINEDIALOG_H
#define STATEMACHINEDIALOG_H

#include <QObject>
#include <QHash>
#include <QDebug>
#include <QSettings>
#include <QVariant>
#include <QTextCodec>
#include "defaults.h"

class StateMachineDialog : public QObject
{
    Q_OBJECT
public:
    explicit StateMachineDialog(Defaults *d, QObject *parent = 0);
    ~StateMachineDialog();

signals:
    void log(const QString &);
    void say(const QString &);
    void function(Defaults::PhraseID,const QHash<Defaults::GroupType,QString> &);

public slots:
    void user_say(const QString &msg);

protected:

private:
    //QStringList phrases, tokens_list;
    //QSettings *lang;
    QHash<Defaults::GroupType,QString> last_context;
    Defaults *defaults;
};

#endif // STATEMACHINEDIALOG_H
