#include "defaults.h"

Defaults::Defaults(QObject *parent) : QObject(parent)
{
    phrases[PhraseID::ObjectMethod] << GroupType::Places << GroupType::Methods << GroupType::Objects;

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(OXCONFPATH+"database.db3");
    db.open();

    QSqlQuery query("SELECT translation FROM i18n");
    while (query.next()) {
        QStringList tokens = query.value(0).toString().split(",");
        tokens_lang << tokens;
    }
    tokens_lang.removeDuplicates();
    qDebug() << tokens_lang;
/*
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QSettings *lang = new QSettings(OXCONFPATH+"global.ini", QSettings::IniFormat);
    lang->setIniCodec(codec);

    QStringList groups = QString("Actions,Methods,Props").split(',');
    foreach (const QString &group, groups){
        lang->beginGroup(group);
        QStringList keys = lang->childKeys();
        foreach (QString key, keys){
            tokens_lang << lang->value(key).toStringList();
            tokens_by_group_lang[group] << lang->value(key).toStringList();
            tokens_by_object_lang[key] << lang->value(key).toStringList();
        }
        objects_by_group[group] << keys;
        lang->endGroup();
    }
    delete lang;

    QDirIterator p(OXCONFPATH, QDir::Dirs | QDir::NoDotAndDotDot);
    while (p.hasNext()) {
        p.next();
        QString place = p.fileName();
        QDirIterator o(OXCONFPATH+place, QStringList() << "*.ini", QDir::Files);
        while (o.hasNext()) {
            o.next();
            QString object = o.fileName();
            QString group_name = "Places";
            lang = new QSettings(OXCONFPATH+place+"/"+object, QSettings::IniFormat);
            lang->setIniCodec(codec);
            lang->beginGroup("Root");
            tokens_lang << lang->value("lang").toStringList();
            if(object!="index.ini"){
                objects_by_place[place] << object.remove(".ini");
                group_name="Objects";
                tokens_by_object_lang[object] << lang->value("lang").toStringList();
                objects_by_group[group_name] << object;
                foreach (const QString &info, lang->allKeys()) {
                    object_info[object].insert(info, lang->value(info).toString());
                }


            }
            else{
                tokens_by_object_lang[place] << lang->value("lang").toStringList();
                objects_by_group[group_name] << place;
            }

            tokens_by_group_lang[group_name] << lang->value("lang").toStringList();
            tokens_by_group_lang[group_name].removeDuplicates();
            lang->endGroup();
            delete lang;
        }
    }
    tokens_lang.removeDuplicates();

    //qDebug() << objects;
    //qDebug() << tokens_by_object_lang;
    //qDebug() << objects_by_group;
    //qDebug() << object_info;
*/
}

bool Defaults::contains(const QString &token){
    return tokens_lang.contains(token);
    return true;
}

QList<Defaults::PhraseID> *Defaults::get_phrases() const{
    QList<Defaults::PhraseID> *ret = new QList<Defaults::PhraseID>();
    *ret << phrases.keys();
    return ret;
}

QList<Defaults::GroupType> *Defaults::phrase_keys(PhraseID key) const{
    QList<Defaults::GroupType> *ret = new QList<Defaults::GroupType>;
    *ret = phrases[key];
    return ret;
}
QStringList *Defaults::tokensForObject(const QString &obj) const{
    QStringList *ret = new QStringList;
    //*ret = tokens_by_object_lang[obj];
    QSqlQuery query;
    query.prepare("SELECT translation FROM i18n WHERE key=:key"); //взять названия объектов
    query.bindValue(":key", obj);
    while (query.next()) {
        QStringList tokens = query.value(0).toString().split(",");
        *ret << tokens;
    }
    //взять дефолтовые названия объектов
    query.prepare("SELECT i18n.translation FROM i18n INNER JOIN Objects ON i18n.key = Objects.qt_class WHERE Objects.var_name = :key");
    query.bindValue(":key", obj);
    while (query.next()) {
        QStringList tokens = query.value(0).toString().split(",");
        *ret << tokens;
    }
    qDebug() << "tokensForObject: " << *ret;
    return ret;
}
QStringList *Defaults::objectsForGroup(Defaults::GroupType g) const{
    QStringList *ret = new QStringList;
    //*ret = objects_by_group[group_name];
    QSqlQuery query;
    switch (g) {
    case GroupType::Objects:
        query.prepare("SELECT var_name FROM Objects");
        break;
    case GroupType::Places:
        delete ret;
        return get_places();
        break;
    case GroupType::Props:
        query.prepare("SELECT some FROM Refs WHERE group_type=:group");
        query.bindValue(":group", g);
        break;
    case GroupType::Methods:
    default:
        query.prepare("SELECT key FROM i18n WHERE group_type=:group");
        query.bindValue(":group", g);
        break;
    }
    while (query.next()) {
        *ret << query.value(0).toString();
    }
    return ret;
}
QStringList *Defaults::get_places() const{
    QStringList *ret = new QStringList;
    QSqlQuery query("SELECT DISTINCT(place) FROM Objects");
    while (query.next()) {
        *ret << query.value(0).toString();
    }
    //qDebug() << "Places: " << *ret;
    return ret;
}
QStringList *Defaults::objectsForPlace(const QString &place_name) const{
    QStringList *ret = new QStringList;
    //*ret = objects_by_place[place_name];
    QSqlQuery query;
    query.prepare("SELECT var_name FROM Objects WHERE place=:place"); //взять названия объектов
    query.bindValue(":place", place_name);
    while (query.next()) {
        *ret << query.value(0).toString();
    }
    return ret;
}
QString *Defaults::classNameByObject(const QString &obj) const{
    QString *ret = new QString;
    QSqlQuery query;
    query.prepare("SELECT qt_class FROM Objects WHERE var_name=:obj"); //взять названия объектов
    query.bindValue(":obj", obj);
    while (query.next()) {
        *ret = query.value(0).toString();
        break;
    }
    //*ret = object_info[obj].value("className");
    return ret;
}
