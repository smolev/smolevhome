#include "roothomeserver.h"

RootHomeServer::RootHomeServer(QObject *parent) : QObject(parent)
{
    defaults = new Defaults(this);

    funcs = new AICodeBase(defaults,this);
    state_dialog = new StateMachineDialog(defaults,this);

    connect(state_dialog,&StateMachineDialog::log,this,&RootHomeServer::log);
    connect(state_dialog,&StateMachineDialog::function,funcs,&AICodeBase::superSwitch);

    state_dialog->user_say("быстро включи телевизор включить");
/*
    speaker = new YaSpeech(this);
    connect(this,&RootHomeServer::say,speaker,&YaSpeech::say);
    connect(speaker,&YaSpeech::log,this,&RootHomeServer::log);

    recogniser = new YaRecognise(this);
    connect(recogniser,&YaRecognise::log,this,&RootHomeServer::log);

    connect(state_dialog,&StateMachineDialog::say,speaker,&YaSpeech::say);
    connect(recogniser,&YaRecognise::say,state_dialog,&StateMachineDialog::user_say);
*/
}

RootHomeServer::~RootHomeServer(){
    delete speaker;
    delete recogniser;
    delete state_dialog;
    delete defaults;
}
void RootHomeServer::log(const QString &msg){
    qDebug() << "log: " << msg;
}

void RootHomeServer::start(){
    log("start");
    speaker->start();
    recogniser->start();
    //emit say("Доброе утро");
}
