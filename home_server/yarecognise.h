#ifndef YARECOGNISE_H
#define YARECOGNISE_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QFile>
#include <QUuid>
#include <QAudioRecorder>
#include <QAudioEncoderSettings>
#include <QAudioProbe>
#include <QUrl>
#include "proto/basic.pb.h"
#include "proto/voiceproxy.pb.h"

class YaRecognise : public QThread
{
    Q_OBJECT
public:
    enum State { Unconnected, Connecting, CanSendAudio };
    Q_ENUM(State)
    YaRecognise(QObject *p);
    ~YaRecognise();

public slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);
    void slotConnected();
    void slotDisconnected();
    void processAudioBuffer(const QAudioBuffer &audioBuffer);

protected:
    void run();
    void bufferProccess();
    void sendChunks(const QAudioBuffer &wav);
    void writeData(const std::string &data);

private:
    QTcpSocket *m_pTcpSocket;
    QByteArray *buffer;
    QAudioRecorder *audioRecorder;
    QAudioProbe *audioProbe;
    State current_state;
    std::string audio_output;
    QHash<QString, QString> biometry;

signals:
    void log(const QString &);
    void say(const QString &);
};

#endif // YARECOGNISE_H
