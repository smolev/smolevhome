#ifndef YASPEACH_H
#define YASPEACH_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QSound>
#include <QString>
#include <QUrl>
#include <QAbstractSocket>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QCryptographicHash>
#include <QDir>
#include <QFileInfo>

class YaSpeech : public QThread
{
    Q_OBJECT
public:
    YaSpeech(QObject *p);
    ~YaSpeech();

signals:
    void log(const QString &);

public slots:
    void say(const QString &msg);

private slots:
    void ttsDownloaded(QNetworkReply* pReply);

protected:
    void run();

private:
    QNetworkAccessManager *m_WebCtrl;
    QSound *audio;
    QString cache_path;
    QString voice, emotion;
};

#endif // YASPEACH_H
