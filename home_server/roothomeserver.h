#ifndef ROOTHOMESERVER_H
#define ROOTHOMESERVER_H

#include <QObject>
#include <QDebug>
#include "yaspeech.h"
#include "yarecognise.h"
#include "statemachinedialog.h"
#include "aicodebase.h"
#include "defaults.h"

class RootHomeServer : public QObject
{
    Q_OBJECT
public:
    explicit RootHomeServer(QObject *parent = 0);
    ~RootHomeServer();
    void start();

signals:
    void say(const QString &);
public slots:
    void log(const QString &msg);

private:
    YaSpeech *speaker;
    YaRecognise *recogniser;
    StateMachineDialog *state_dialog;
    AICodeBase *funcs;
    Defaults *defaults;
};

#endif // ROOTHOMESERVER_H
