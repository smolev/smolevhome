#!/bin/bash

#rsync -r /etc/openhab2/ openhab2
rsync -r openhab2/ /etc/openhab2
chown -R pi.pi openhab2/
chmod -R 755 openhab2/
cp -alf openhab2 /etc/
chown -R openhab.openhab /etc/openhab2/
chmod -R 755 /etc/openhab2/
