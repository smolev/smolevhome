#!/usr/bin/ruby
require 'serialport'
START_BT=171
STOP_BT=172
@ser = SerialPort.new("/dev/ttyAMA0",9600,8,1,SerialPort::NONE)
@ser.read_timeout = 2000
@ser.sync = true
# W :{171, 2, 0, 0, 4, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 192, 172}
# R :{173, 2, 3, 0, 4, 0, 0, 1, 0, 0, 0, 0, 0, 48, 120, 95, 174}
class Array
  def chr
    self.map {|e|e.chr}
  end
end

def noo_raw_command(arr)
    crc=arr.inject(0, :+)+START_BT
	crc=crc - 0x100 if crc > 0x100
    while arr.size < 14
	arr = arr + [0]
    end
    ret = [START_BT]+arr+[crc,STOP_BT]
    puts "# W :{#{ret.join(", ")}}"
    return ret
end

def noo_pair(channal)
    return noo_raw_command([2,0,0,channal,15])
end

def noo_off(channal)
    return noo_raw_command([2,0,0,channal])
end

def noo_on(channal)
	#25 - включить на время
    return noo_raw_command([2,0,0,channal,2])
end

def send_command(cmd)
    cmd.each do |c|
		@ser.write c.chr
    end
end

def fast_read
    c=@ser.read(17)
	if c==nil
		puts "# R timeout"
	else	
		puts "# R :{#{c.bytes.join(", ")}}"
	end
end

send_command noo_raw_command([4])
fast_read()
#send_command noo_pair(4)
#noo_off(4)

while true do
    puts "on"
    send_command noo_on(4)
    #fast_read()
    sleep(5)
    puts "off"
    send_command noo_off(4)
    #fast_read()
    sleep(5)
end

@ser.close
