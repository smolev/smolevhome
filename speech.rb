#!/usr/bin/ruby
# curl "https://tts.voicetech.yandex.net/generate?format=mp3&lang=ru-RU&speaker=ermil&emotion=good&key=1578a96c-8f40-4861-9bd4-4a65e4e0400f" -G --data-urlencode "text=Свет в туалете выключен" > speech3.mp3
require "net/http"
require 'sndfile-ruby'

def ya_say(message, emotion="good")
	uri = URI.parse("https://tts.voicetech.yandex.net/generate")
	args = {
		key: "1578a96c-8f40-4861-9bd4-4a65e4e0400f", format: "wav", lang: "ru-RU", speaker: "ermil",
		text: message,
		emotion: emotion
	}
	uri.query = URI.encode_www_form(args)
	http = Net::HTTP.new(uri.host, uri.port)
	http.use_ssl = true

	request = Net::HTTP::Get.new(uri.request_uri)

	response = http.request(request)
	return response.body
end

#EasyAudio.easy_open(in: true, out: true, latency: 1.0) { ya_say("Съешь еще этих мягких булочек") }
#EasyAudio.easy_open(&EasyAudio::Waveforms::SINE)
#EasyAudio.easy_open(ya_say("Съешь еще этих мягких булочек").to_a)
sleep 10 # for 10 seconds

#File.write("ss.mp3", ya_say("Съешь еще этих мягких булочек"))

# YandexSpeechApi::Key.global_key = "1578a96c-8f40-4861-9bd4-4a65e4e0400f"
# YandexSpeechApi::Speaker::Tempfile = "ss.mp3"

# message = "Съешь еще этих булочек"

# speaker = YandexSpeechApi::Speaker.init
# speaker.key      = "1578a96c-8f40-4861-9bd4-4a65e4e0400f"
# speaker.voice    = :ermil
# #speaker.temp_file = "sss.mp3"
# #speaker.speed    = 1.2
# speaker.emotion  = 'good'
# speaker.save_to_file message, '~/downloads/sound'